# WinHandle64ParserOutput

Example of how to recover files opened by a process via the output of Handle64.exe

**Download** the **Handle64.exe** executable : (https://docs.microsoft.com/en-us/sysinternals/downloads/handle)

## Powershell 

```powershell
    .\handle64.exe -p 552 | Select-String "File" | ForEach {(([string]$_).Split(' ', 12)[11]).TrimStart()} | Sort | Unique
    .\handle64.exe firefox | Select-String "File" | ForEach {(([string]$_).Split(':', 4)[3]).TrimStart()} | Sort | Unique
```